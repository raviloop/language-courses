@extends('layouts.master') 
@section('title','Search Results') 
@section('content') 

<div class="search-page-content">

    <div class="search-page-search-box">
        <input type="text" placeholder="search" />
        <button>GO</button>        
    </div>

    <div class="search-results">
    
        <h3>Search Results </h3>
        <div class="search-results-box">
            <div class="search-result-box">Box 1</div>
            <div class="search-result-box">Box 2</div>
            <div class="search-result-box">Box 3</div>
            <div class="search-result-box">Box 4</div>
            <div class="search-result-box">Box 5</div>
            <div class="search-result-box">Box 6</div>
            <div class="search-result-box">Box 7</div>
            <div class="search-result-box">Box 8</div>
        </div>
    
    </div>

</div>

@endsection