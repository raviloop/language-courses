@extends('layouts.master') 
@section('title','Home') 
@section('content')

<div class="home-content">
    <div class="home-title">
        <p>Language Learning</p>
    </div>
    <div class="search-box">
        {{-- <input type="search" class="search_input" placeholder="Search" /> --}}
        <input type="search" class="search_input" placeholder="Search" />
        <button class="btn btn-primary search_button" onclick="window.location='{{ url("search-page") }}'">Search</button>
    </div>
</div>

<div class="top-courses">

    <h3> Top Courses</h3>
    <div class="box-container">
        <div class="course-box">Box 1</div>
        <div class="course-box">Box 2</div>
        <div class="course-box">Box 3</div>
        <div class="course-box">Box 4</div>
        <div class="course-box">Box 5</div>
        <div class="course-box">Box 6</div>
        <div class="course-box">Box 7</div>
        <div class="course-box">Box 8</div>
    </div>

</div>


<div class="top-courses">

    <h3> Ranking Of Most Popular Courses</h3>
    <div class="box-container">
        <div class="course-box">Box 1</div>
        <div class="course-box">Box 2</div>
        <div class="course-box">Box 3</div>
        <div class="course-box">Box 4</div>
        <div class="course-box">Box 5</div>
        <div class="course-box">Box 6</div>
        <div class="course-box">Box 7</div>
        <div class="course-box">Box 8</div>
    </div>

</div>
@endsection
 
@section('page-js-script')
<script type="text/javascript">
    $(document).ready(function() {
        console.log("home page was loaded");
    });

</script>



@stop