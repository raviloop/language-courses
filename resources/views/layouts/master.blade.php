<!DOCTYPE html>
<html>
<head>
    @include('includes.head')
</head>
<body>
    <header>
        @include('includes.header')
    </header>
<div class="container-fluid">


{{--         <div id="sidebar" class="col-md-4">
            @include('includes.sidebar')
        </div>
 --}}
    <div id="main" class="row">

            @yield('content')

    </div>

    <footer class="row">
        @include('includes.footer')
    </footer>

</div>
</body>

  @yield('page-js-script')

</html>