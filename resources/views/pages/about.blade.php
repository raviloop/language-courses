@extends('layouts.master')

@section('title', 'About')

@section('content')
<h1>About</h1>
<p>This is the application I'm building in the 
  <a href="https://selftaughtcoders.com/from-idea-to-launch">
  From Idea To Launch</a> course.</p>
<p>This web application will allow people to 
  both submit, and answer, programming-related 
  questions for various different 
  programming languages.</p>
@endsection