<meta charset="utf-8">
<meta name="description" content="">
<meta name="author" content="Courses">

<title>LC - @yield('title')</title>

<!-- load bootstrap from a cdn -->
 <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap.min.css')}}"/> 
 <link rel="stylesheet" type="text/css" href="{{url('css/home.css')}}"/> 
 <link rel="stylesheet" type="text/css" href="{{url('css/search-page.css')}}"/> 

<script src="{{url('js/bootstrap.min.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
