<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});

Route::get('/about', function () {
    return view('pages.about');
});


Route::get('/search-page', function () {
    return view('pages.search-page');
});


Route::get('/course-detail', function () {
    return view('pages.course-detail');
});

Route::get('/courses', function () {
    return view('pages.courses');
});

Route::get('/contact', function () {
    return view('pages.contact');
});

Route::get('testing','TestController@test_db');